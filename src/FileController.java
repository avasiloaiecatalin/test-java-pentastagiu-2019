import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileController {

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(boolean errorStatus) {
        this.errorStatus = errorStatus;
    }

    private boolean errorStatus;
    private String filePath;

    public FileController(String fP){
        setFilePath(fP);
    }

    public void write(String content){
        BufferedWriter bw = null;
        try{
            bw = new BufferedWriter(new FileWriter(filePath,true));
            bw.write(content);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            errorStatus = true;
        }
        finally {
            try {
                if (bw != null)
                    bw.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
                errorStatus = true;
            }
        }
    }
}
