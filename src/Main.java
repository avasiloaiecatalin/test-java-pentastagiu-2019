/*
AVASILOAIE CATALIN
" STEFAN CEL MARE " UNIVERSITY OF SUCEAVA
 */

import java.util.Scanner;

public class Main {
    private  static final String no = "N";
    public static void main(String[] args) {
        FileController fc = new FileController("fileToWrite/out.txt");
        Scanner scan = new Scanner(System.in);
        String option;
        do{
            System.out.println("Do you want to write content? [Y/N]");
            option = scan.nextLine().toUpperCase();
            switch (option){
                case "Y":
                    String content = scan.nextLine();
                    System.out.println("Content: "+content);
                    fc.write(content+"\n");
                    break;
                case "N":
                    return;
            }
            if(fc.isErrorStatus())
                return;
        }while(option != no);
    }
}
